
    <td>
        <p><strong><?php echo $row->name; ?></strong></p>
    </td>
    <td>
        <?php echo $row->quantity;?>
    </td>
    <td> 
        @if (is_null($row->conditions))
            {{$row->price}}{{$currency}}
        @else
            @if($row->getPriceWithConditions()!=$row->price)
                <del>{{$row->price}}{{$currency}}</del>{{$row->getPriceWithConditions()}}{{$currency}}
            @else
                {{$row->price}}{{$currency}}
            @endif
        @endif    
    </td>
    <td>
        {{$currency}}
        @if (is_null($row->conditions))
            {{$row->getPriceSum()}} 
        @else
            {{$row->getPriceSumWithConditions()}}
        @endif
    </td>

        



