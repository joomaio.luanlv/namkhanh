@extends('layouts.app')

@section('title', 'Checkout')

@section('content')
  <div style="height: 68px;"></div>

  <div class="container mt-3">
    <div class="row">
      <div class="col-lg-12 text-center">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>	
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div><br />
        @endif 
      </div>
    </div>
  </div>   

  <div class="container my-5">
    @if (!Auth::user())
      <div class="row mb-5">
        <div class="col-12 col-md-12">
          {{-- <div class="alert alert-primary" role="alert">
            Returning customer? <a href="#" id="click_to_login" class="alert-link">click here to login</a>
          </div> --}}

          <div class="card border-light d-none" id="checkout_login">
            <div class="card-body">
              {{-- <p>If you have shopped with us before, please enter your details below. If you are a new customer, please proceed to the Billing & Shipping section. --}}
              {{-- </p> --}}
              <form action="{{route('checkout.login')}}" method="post">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="mb-3">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" name="email">
                    </div>
                  </div>
                
                  <div class="col-md-6">
                    <div class="mb-3">
                      <label for="email">Mật khẩu</label>
                      <input type="password" class="form-control" name="password">
                    </div>
                  </div>
                </div>
                
                <button type="submit" class="btn btn-lg btn-primary">Đăng nhập</button>
              </form>
            </div>
          </div>
        </div>
      </div> 
    @endif
    <div class="row">  
      <?php 
      $currency=get_current_currency()['symbol'];
      if(!$customer){
        $billing_country='';
        $billing_address='';
        $billing_city='';
        $billing_state='';
        $billing_zip='';
        $shipping_country='';
        $shipping_address='';
        $shipping_city='';
        $shipping_state='';
        $shipping_zip='';
      }
      else{
        $billing_country=$customer->billing_country;
        $billing_address=$customer->billing_address;
        $billing_city=$customer->billing_city;
        $billing_state=$customer->billing_state;
        $billing_zip=$customer->billing_zip;
        $shipping_country=$customer->shipping_country;
        $shipping_address=$customer->shipping_address;
        $shipping_city=$customer->shipping_city;
        $shipping_state=$customer->shipping_state;
        $shipping_zip=$customer->shipping_zip;
      }

      if(Auth::user()){
        $name=auth()->user()->name;
        $email=auth()->user()->email;
      }
      else{
        $name=old('name');
        $email=old('email');
      }
                 
      ?>
      <div class="col-md-4 order-md-2 mb-4">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-muted">Giỏ hàng của bạn</span>
        <span class="badge badge-secondary badge-pill">{{Cart::getTotalQuantity()}}</span>
        </h4>
        <ul class="list-group mb-3">
          <?php $sum=0;?>
          @foreach (Cart::getContent() as $row)
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">{{$row->name}} ({{$row->quantity}})</h6>
              </div>
              <span class="text-muted">
                @if ($row->getPriceWithConditions()!=$row->price)
                  <del>{{$row->price}}{{$currency}}</del>{{$row->getPriceWithConditions()}}{{$currency}}
                @else
                  {{$row->price}}{{$currency}}
                @endif
              </span>
            </li>
            <?php $sum=$sum+$row->getPriceSumWithConditions();?>
          @endforeach
          
          @if(count(Cart::getConditionsByType('coupon'))!=0)          
          <li class="list-group-item d-flex justify-content-between bg-light">
            <div class="text-success">
              <h6 class="my-0">Mã giảm giá (<a href="/cart/discountremove">Xóa</a>)</h6>
              <small>{{Cart::getConditionsByType('coupon')->first()->getName()}}</small>
            </div>
            <span class="text-success">-{{two_decimal(Cart::getConditionsByType('coupon')->first()->getCalculatedValue($sum))}}{{$currency}}</span>
          </li>
          @endif
          <li class="list-group-item d-flex justify-content-between">
            <span>Tổng</span>
            <strong>{{Cart::getSubTotal()}}{{$currency}}</strong>
          </li>
          {{-- <?php $cartConditions = Cart::getConditions(); ?>
          @if (count($cartConditions)>0)
            @foreach($cartConditions as $condition)
              @if ($condition->getType()!='coupon')
                  <li class="list-group-item d-flex justify-content-between">
                    <span>{{$condition->getName()}}</span>
                    <strong>{{two_decimal($condition->getCalculatedValue(Cart::getSubTotal()))}}{{$currency}}</strong>
                  </li>
              @endif
            @endforeach
          @endif --}}
          
          {{-- <li class="list-group-item d-flex justify-content-between">
            <span>Tổng</span>
            <strong>{{Cart::getTotal()}}{{$currency}}</strong>
          </li> --}}
        </ul>

        <form class="card p-2" action="{{route('cart.add-discount')}}" method="get">
          @csrf
          <div class="input-group">
            <input type="text" class="form-control" name="discount" placeholder="Mã giảm giá">
            <div class="input-group-append">
              <button type="submit" class="btn btn-secondary">Áp dụng</button>
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-8 order-md-1">
        <form id="billing-form" action="{{route('checkout.process')}}" method="post">
          @csrf
          <h4 class="mb-3">Thông tin thanh toán</h4>
          <div class="mb-3">
            <label for="name">Tên đầy đủ</label>
            <input type="name" class="form-control" name="name" id="name" value="{{$name}}" placeholder="Nhập tên của bạn">
          </div>

          <div class="mb-3">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" value="{{$email}}" placeholder="Nhập email">
          </div>

          <div class="mb-3">
            <label for="number">Số điện thoại</label>
            <input type="number" class="form-control" name="number" id="number" placeholder="Nhập số điện thoại">
          </div>

          <div class="mb-3">
            <label for="address">Địa chỉ giao hàng</label>
            <input type="text" class="form-control" name="address" id="address" value="{{$billing_address}}" placeholder="Nhập địa chỉ giao hàng">
          </div>

          {{-- <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label class="form-control-label" for="country">Quốc gia</label>
                <select name="country" id="country">
                  @foreach ($countries as $country)
                    <option value="{{$country}}" {{$billing_country==$country?'selected':''}}>{{$country}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div> --}}

          <div class="row">
            {{-- <div class="col-md-4 mb-3">
              <label for="city">Thành phố</label>
              <input type="text" class="form-control" name="city" id="city" value="{{$billing_city}}" placeholder="">
            </div>
            <div class="col-md-4 mb-3">
              <label for="state">Quận/huyện</label>
              <input type="text" class="form-control" name="state" id="state" value="{{$billing_state}}" placeholder="">
            </div> --}}
            <div class="col-md-4 mb-3 d-none">
              <label for="zip">Mã bưu điện</label>
              <input type="number" class="form-control" name="zip" id="zip" value="03330" placeholder=""> 
            </div>
          </div>
          {{-- {{$billing_zip}} --}}
          
          {{-- <hr class="mb-4"> --}}

          {{-- <div class="row mb-4">
            <div class="col-md-12">
              <input id="same-address" name="shipping_address_different" type="checkbox">
              <label class="custom-control-label" for="same-address">Địa chỉ giao hàng khác?</label>
            </div>
          </div> --}}
          
          {{-- @include('sections.shipping') --}}
          <hr class="mb-4">
          <div class="row"></div>
          
          <button type="submit" class="btn btn-primary btn-lg btn-block">Thanh toán</button>
        </form>
          
        </div>
      </div>
    </div>
  </div>
 
  
@stop

@section('javascript')

  <script src="/frontend/assets/js/checkout.js"></script>

@endsection