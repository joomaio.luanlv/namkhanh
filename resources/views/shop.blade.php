@extends('layouts.app')

@section('title', 'Shop')

@section('content')
    <!-- Hero -->
    <section class="section-header text-white" style="padding-bottom: 10px;padding-top: 8rem;">
        <div class="container">
            <div class="row pb-2">
                <div class="d-flex justify-content-between" style="width: 100%;">
                    <button type="button" class="btn btn-outline-info btn-sm">Khuyến mãi hot</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Thương hiệu</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Sản phẩm mới</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Trang điểm</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Dưỡng da</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Chăm sóc cá nhân</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Chăm sóc cơ thể</button>
                    <button type="button" class="btn btn-outline-info btn-sm">Nhận mã ưu đãi</button>
                    {{-- <button type="button" class="btn btn-outline-info btn-sm">Chăm sóc cá nhân</button> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 p-1">
                    <div class="swiper mySwiper1" style="width: 100%;border-radius: 10px;">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <img src="/frontend/assets/img/banner/bn1.png" alt="banner">
                        </div>
                        <div class="swiper-slide">
                            <img src="/frontend/assets/img/banner/bn2.png" alt="banner">
                        </div>
                        <div class="swiper-slide">
                            <img src="/frontend/assets/img/banner/bn1.png" alt="banner">
                        </div>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    
                        <!-- If we need scrollbar -->
                        <div class="swiper-scrollbar"></div>
                    </div>
                </div>
                <div class="col-md-4 p-1">
                    <div class="mb-1" >
                        <img style="border-radius: 10px;" src="/frontend/assets/img/banner/bn3.png" alt="banner">
                    </div>
                    <div>
                        <img style="border-radius: 10px;" src="/frontend/assets/img/banner/bn4.png" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-soft pb-5 pt-4">
        <div class="container mb-3">
            @if (request()->input('query') && !request()->input('category'))
                <h6 class="p-0 m-0">Kết quả tìm kiếm cho: <span style="color: red;font-style: italic;">{{request()->input('query')}}</span></h6>
            @endif
            <div class="row p-2 mt-2" style="background: rgba(0,0,0,.03);">
                <div class="col-1 p-0 d-flex align-items-center">
                    Sắp xếp theo
                </div>
                <div class="col-2">
                    <select class="custom-select custom-select-sm" name="price">
                        <option selected>Giá bán</option>
                        <option value="1">Từ thấp đến cao</option>
                        <option value="2">Từ cao đến thấp</option>
                    </select>
                </div>
                <div class="col-2 p-0">
                    <select class="custom-select custom-select-sm" name="create">
                        <option selected>Mới nhập</option>
                        <option value="1">Mới nhất</option>
                        <option value="2">Cũ nhất</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="container">

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>	
                <strong>{{ $message }}</strong>
            </div>
            @endif
            
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif 

            <?php $currency=get_current_currency()['symbol'];?>

            <div class="row">
                <div class="col-lg-9 push-md-3">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                @foreach ($products as $product)
                                    <div class="col-md-4">
                                        <div class="card card-product card-plain">
                                            <div class="card-image">
                                                <a href="/product/{{$product->slug}}">
                                                    <img src="{{is_null($product->primary_image)?'/frontend/assets/img/default.png':Voyager::image($product->primary_image)}}" />
                                                </a>
                                            </div>
                                            <div class="card-body p-0 m-0">
                                                
                                                <h4 class="mt-3"><a href="/product/{{$product->slug}}">{{$product->title}}</a></h4>
                                                <p class="text-muted">{{is_null($product->category)?"Uncategorized":$product->category->name}}</p>
                                                <p>
                                                    @if ($product->discounted_price>0)
                                                        <del>{{$product->regular_price}}{{$currency}}</del> {{$product->discounted_price}}{{$currency}}   
                                                    @else
                                                        {{$product->regular_price}}{{$currency}} 
                                                    @endif
                                                </p>
                                                <div class="d-flex mb-4">
                                                    <?php $sum=0; ?>
                                                    @foreach ($product->reviews as $review)
                                                        <?php $sum=$sum+$review->rating;?>
                                                    @endforeach
                                                    <?php 
                                                    if(count($product->reviews)>0){
                                                        $average=$sum/count($product->reviews);
                                                    }
                                                    else{
                                                        $average=0;
                                                    }
                                                    ?>
                                                    <?php $review_rate=$average; ?>
                                                    
                                                    {{--Start Rating--}}
                                                    @for ($i = 0; $i < 5; $i++)
                                                    @if (floor($review_rate) - $i >= 1)
                                                        {{--Full Start--}}
                                                        <i class="fas fa-star text-warning"> </i>
                                                    @elseif ($review_rate - $i > 0)
                                                        {{--Half Start--}}
                                                        <i class="fas fa-star-half-alt text-warning"> </i>
                                                    @else
                                                        {{--Empty Start--}}
                                                        <i class="far fa-star text-warning"> </i>
                                                    @endif
                                                    @endfor
                                                    {{--End Rating--}}  
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end card -->
                                    </div>
                                @endforeach 
                            </div>
                        </div>
                    </div>
                          
                    <div class="mt-3 text-center">
                        {{$products->appends(request()->query())->links()}}
                    </div>
                </div>
                <div class="col-lg-3 pull-md-9">
                    <form class="form-inline" action="{{route('shop.search',['query'=>request()->input('query')])}}">
                        <div class="md-form my-0">
                            <input class="form-control mr-sm-2" name="query" value="{{request()->input('query')}}" type="text" placeholder="Tìm kiếm" aria-label="Search">
                        </div>
                        <div class="ml-1">
                            <h4 class="mt-3">Danh mục sản phẩm</h4>
                        
                            <ul class="list-unstyled">
                                @if($categories->count()>0)
                                    @foreach($categories as $category)  
                                        <li class="d-flex align-items-center">
                                            {{-- @if(request()->input('catId')) --}}
                                                @if(request()->input('catId') && in_array($category->id, request()->input('catId')))
                                                    <input type="checkbox" checked name="catId[]" id="" value="{{$category->id}}" style="width: 20px;height: 20px;"> 
                                                @else
                                                    <input type="checkbox" name="catId[]" id="" value="{{$category->id}}" style="width: 20px;height: 20px;"> 
                                                @endif
                                            
                                            {{-- @endif --}}
                                            <p class="px-3 m-0">{{$category->name}}</p>
                                        </li>   
                                    @endforeach
                                @else
                                    Chưa có danh mục nào
                                @endif
                            </ul>
                        </div>
                        <div class="ml-1">
                            <h4 class="mt-3">Khoảng giá</h4>

                            <div class="d-flex">
                                <input type="text" maxlength="13" placeholder="₫ TỪ" name="from" value="{{request()->input('from')}}" style="width: 95px;height: 30px;">
                                <div class="mx-2">-</div>
                                <input type="text" maxlength="13" placeholder="₫ ĐẾN" value="{{request()->input('to')}}" name="to" style="width: 95px;height: 30px;">
                            </div>

                            <button class="mt-3" type="submit" style="background-image: linear-gradient(to left, blue, orange, red);font-weight: 400;width: 100%;padding: 5px 0;border-radius: 2px;border: 0px solid white;color: white;">ÁP DỤNG</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
    <section class="bg-soft pb-5 pt-4">
        <div class="container">
            <div class="row mb-2" style="padding: 0 15px;">
                <h4 class="title"><strong style="color: red;">Sản phẩm</strong> Nội bật</h4>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        @foreach ($featured_products as $product)
                            <div class="col-md-4">
                                <div class="card card-product card-plain">
                                    <div class="card-image">
                                        <a href="/product/{{$product->slug}}">
                                            <img src="{{is_null($product->primary_image)?'/frontend/assets/img/default.png':Voyager::image($product->primary_image)}}" />
                                        </a>
                                    </div>
                                    <div class="card-body p-0 m-0">
                                        
                                        <h4 class="mt-3"><a href="/product/{{$product->slug}}">{{$product->title}}</a></h4>
                                        <p class="text-muted">{{is_null($product->category)?"Uncategorized":$product->category->name}}</p>
                                        <p>
                                            @if ($product->discounted_price>0)
                                                <del>{{$product->regular_price}}{{$currency}}</del> {{$product->discounted_price}}{{$currency}}   
                                            @else
                                                {{$product->regular_price}}{{$currency}} 
                                            @endif
                                        </p>
                                        <div class="d-flex mb-4">
                                            <?php $sum=0; ?>
                                            @foreach ($product->reviews as $review)
                                                <?php $sum=$sum+$review->rating;?>
                                            @endforeach
                                            <?php 
                                            if(count($product->reviews)>0){
                                                $average=$sum/count($product->reviews);
                                            }
                                            else{
                                                $average=0;
                                            }
                                            ?>
                                            <?php $review_rate=$average; ?>
                                            
                                            {{--Start Rating--}}
                                            @for ($i = 0; $i < 5; $i++)
                                            @if (floor($review_rate) - $i >= 1)
                                                {{--Full Start--}}
                                                <i class="fas fa-star text-warning"> </i>
                                            @elseif ($review_rate - $i > 0)
                                                {{--Half Start--}}
                                                <i class="fas fa-star-half-alt text-warning"> </i>
                                            @else
                                                {{--Empty Start--}}
                                                <i class="far fa-star text-warning"> </i>
                                            @endif
                                            @endfor
                                            {{--End Rating--}}  
                                        </div>
                                    </div>
                                </div>
                                <!-- end card -->
                            </div>
                        @endforeach 
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascript')
<script>
    var swiper1 = new Swiper(".mySwiper1", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
    });

    var swiper2 = new Swiper(".mySwiper2", {
        slidesPerView: 5,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });
</script>
@stop