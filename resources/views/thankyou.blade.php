@extends('layouts.app')

@section('title', 'Thank you')

@section('content')
    <!-- Hero -->
    <section class="section-header text-white" style="    background: linear-gradient(90deg, rgb(102, 54, 149) 0%, rgb(199, 49, 48) 50.52%, rgb(255, 212, 0) 99.61%);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 text-center">
                    <h1 class="display-2 mb-3">Hoàn thành đơn hàng</h1>
                    <p class="lead">Đơn hàng của bạn đã hoàn thành</p>
                </div>
            </div>
        </div>
    </section>
    <div class="section py-7 bg-soft">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                <h2>Cảm ơn bạn đã mua hàng</h2>
                <a href="/" class="btn btn-lg btn-primary">Về trang chủ</a>
                </div>
            </div>
        </div>
    </div>
@endsection