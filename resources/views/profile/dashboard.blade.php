@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

  <!-- Hero -->
  {{-- <section class="bg-primary text-white pt-7 pb-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 text-center">
              <h1 class="display-2 mb-3">Trang chủ</h1>
              <p class="lead">Đây là trang chủ</p>
            </div>
        </div>
    </div>
  </section> --}}
  <div style="height: 90px;"></div>

  <section class="section py-5 bg-soft">
    <div class="container">
      <div class="row justify-content-center">
  
        @include('partials.dashboardsidebar')

        <div class="col-md-8">
            <div class="card border-light">
                <div class="card-header">Trang chủ</div>

                <div class="card-body">
                    @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                    @endif

                    Đây là trang thông tin cá nhân của bạn.
                </div>
            </div>
        </div>
    </div>
    </div>
  </section>
  

@endsection
