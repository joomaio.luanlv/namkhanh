@extends('layouts.app')

@section('title', 'Address info')

@section('content')

  <!-- Hero -->
  <div style="height: 90px;"></div>

  <section class="section py-5 bg-soft">
    <div class="container">
      <div class="row justify-content-center">
  
        @include('partials.dashboardsidebar')

        <div class="col-md-8">
            <div class="card border-light">
                {{-- <div class="card-header">Sửa địa chỉ</div> --}}

                <div class="card-body">
                    @if ($message = Session::get('success'))
                      <div class="alert alert-success alert-block">
                          <button type="button" class="close" data-dismiss="alert">×</button>	
                          <strong>{{ $message }}</strong>
                      </div>
                    @endif
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div><br />
                    @endif
                    
                    <?php 
                    if($customer){
                        $billing_address= $customer->billing_address;
                        $billing_city= $customer->billing_city;
                        $billing_state= $customer->billing_state;
                        $billing_zip= $customer->billing_zip;
                        $billing_country= $customer->billing_country;
                        $shipping_address= $customer->shipping_address;
                        $shipping_city= $customer->shipping_city;
                        $shipping_state= $customer->shipping_state;
                        $shipping_zip= $customer->shipping_zip;
                        $shipping_country= $customer->shipping_country;
                    }
                    else{
                        $billing_address= '';
                        $billing_city= '';
                        $billing_state= '';
                        $billing_zip= '';
                        $billing_country='';
                        $shipping_address= '';
                        $shipping_city= '';
                        $shipping_state= '';
                        $shipping_zip= '';
                        $shipping_country='';
                    }
                    ?>
                    <form action="{{route('profile.billing')}}" method="post">
                      @csrf
                      <h6 class="heading-small text-muted mb-4">Sửa địa chỉ</h6>
                      <div class="pl-lg-4">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <label class="form-control-label" for="input-username">Địa chỉ giao hàng</label>
                              <input type="text" name="billing_address" class="form-control" value="{{$billing_address}}">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <button type="submit" class="btn btn-info">Lưu</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <hr class="my-4" />
                  </div>
                </div>
            </div>
        </div>
    </div>
    </div>
  </section>
  

@endsection
