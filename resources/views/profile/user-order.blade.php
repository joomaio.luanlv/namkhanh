@extends('layouts.app')

@section('title', 'Orders')

@section('content')

    <!-- Hero -->
    {{-- <section class="bg-primary text-white pt-7 pb-4">
    </section> --}}
    <div style="height: 90px;"></div>
   
    <div class="section py-5 bg-soft">
        <div class="container">
            <div class="row justify-content-center">

                @include('partials.dashboardsidebar')

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Đơn hàng</div>

                        <div class="card-body">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                            @endif
                            <div class="box-body table-responsive-sm no-padding">    
                                <table class="table table-bordered" id="orders-table">
                                    <thead>
                                        <tr>
                                            <th>Mã đơn hàng</th>
                                            <th>Địa chỉ giao hàng</th>
                                            <th>Trạng thái</th>
                                            <th>Thời gian đặt hàng</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@stop

@section('javascript')
    <script src="/frontend/assets/js/user-order.js"></script>   
@endsection
