<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Srmklive\PayPal\Services\ExpressCheckout;

use Cart;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Countries\Package\Countries;

class PayPalController extends Controller
{
    /**
     * @var ExpressCheckout
     */
    protected $provider;

    public function __construct()
    {
        $this->provider = new ExpressCheckout();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getExpressCheckout(Request $request)
    {
        session(['cost_id' => $request->id]);
        session(['url_prev' => url()->previous()]);
        $vnp_TmnCode = "1Q9S9R9A"; //Mã website tại VNPAY 
        $vnp_HashSecret = "AGJMYFMSMFZBLUUVVDBFRLWGWVYXTNXJ"; //Chuỗi bí mật
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://127.0.0.1:8000/payment-vnpay-return";
        $vnp_TxnRef = date("YmdHis"); //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = "Thanh toán hóa đơn phí dich vụ";  
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = 10000;
        $vnp_Locale = 'vn';
        $vnp_IpAddr = request()->ip();

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        if (isset($vnp_Bill_State) && $vnp_Bill_State != "") {
            $inputData['vnp_Bill_State'] = $vnp_Bill_State;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//  
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array('code' => '00'
            , 'message' => 'success'
            , 'data' => $vnp_Url);
            if (isset($_POST['redirect'])) {
                header('Location: ' . $vnp_Url);
                die();
            } else {
                echo json_encode($returnData);
            }
        return redirect($vnp_Url);

        // if(Cart::isEmpty()){
        //     return redirect('/cart');
        // }
        // session()->put('payment_method',['name'=>'paypal','id'=>NULL]);
        // return redirect(route('thankyou'))->with('success','thankyou');
    }

    /**
     * Process payment on PayPal.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getExpressCheckoutSuccess(Request $request)
    {
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        $cart = $this->getCheckoutData();

        // Verify Express Checkout Token
        $response = $this->provider->getExpressCheckoutDetails($token);

        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            
            // Perform transaction on PayPal
            $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
            $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
            

            $invoice = $this->createInvoice($cart, $status);

            if ($invoice['paid']) {
                return redirect(route('thankyou'))->with('success','processed');
            } else {
                session()->forget('payment_method');
                return redirect('/checkout')->withErrors('Error payment processing');            
            }
           
        }
        
    }
    
    /**
     * Set cart data for processing payment on PayPal.
     *
     * @param bool $recurring
     *
     * @return array
     */
    protected function getCheckoutData()
    {
        $data = [];

        Config::set('paypal.currency',get_current_currency()['code']);
        $order_id = uniqid();
        session()->put('payment_method',['name'=>'paypal','id'=>$order_id]);

        $cart_items=\Cart::getContent();

        $data['items'] = collect([]);

        $sum=0;
        foreach($cart_items as $row){
            $sum=$sum+$row->getPriceSumWithConditions();
            $data['items']->push(
                array(
                    'name'  => $row->name,
                    'price' => $row->getPriceWithConditions(),
                    'qty'   => $row->quantity 
                )
            );
        }
        
        $data['return_url'] = url('/paypal/ec-checkout-success');
        

        $data['invoice_id'] = config('paypal.invoice_prefix').'_'.$order_id;
        $data['invoice_description'] = "Order #$order_id Invoice";
        $data['cancel_url'] = route('checkout');

        if(count(\Cart::getConditionsByType('coupon'))!=0){
            
            $discount=\Cart::getConditionsByType('coupon')->first()->getCalculatedValue($sum);

            $sub_after_discount=$sum-$discount;

            if($sub_after_discount<0){
                $data['items']->push(
                    array(
                        'name'  => 'Discount',
                        'price' => '-'.$sum,
                        'qty'   => 1 
                    )
                );
            }
            else{
                $data['items']->push(
                    array(
                        'name'  => 'Discount',
                        'price' => '-'.$discount,
                        'qty'   => 1 
                    )
                );
            }
 
        }
        
        
        $data['subtotal'] = \Cart::getSubTotal();
        
        if(count(\Cart::getConditions())>0){
            $total = 0;
            foreach (\Cart::getConditions() as $condition) {
                if ($condition->getType()!='coupon'){
                    $total =$total+$condition->getCalculatedValue(\Cart::getSubTotal());
                }
            }

            $data['shipping'] = $total;

        }
        
        $data['total'] = \Cart::getTotal();

        return $data;
    }

    /**
     * Create invoice.
     *
     * @param array  $cart
     * @param string $status
     *
     * @return \App\Invoice
     */
    protected function createInvoice($cart, $status)
    {
        $invoice=[];
        $invoice['title'] = $cart['invoice_description'];
        $invoice['price'] = $cart['total'];
        if (!strcasecmp($status, 'Completed') || !strcasecmp($status, 'Processed')) {
            $invoice['paid'] = 1;
        } else {
            $invoice['paid'] = 0;
        }
        
        return $invoice;
    }
    
}