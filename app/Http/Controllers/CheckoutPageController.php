<?php

namespace App\Http\Controllers;

use Cart;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Countries\Package\Countries;

class CheckoutPageController extends Controller
{
    public function index(){
        if(Cart::isEmpty()){
            return redirect('/cart');
        }

        if(Auth::user()){
            $customer=Customer::where('user_id',Auth::user()->id)->first();
        }
        else{
            $customer=NULL;
        }

        $countries=Countries::all()->pluck('name.common');
        return view('checkout',compact('customer','countries'));
    }
    public function payment(){
        if(!session()->has('success')){
            return redirect('/cart');
        }
        return view('payment');
    }
    
    public function processCod(){
        if(Cart::isEmpty()){
            return redirect('/cart');
        }
        session()->put('payment_method',['name'=>'cod','id'=>NULL]);
        return redirect(route('thankyou'))->with('success','thankyou');
    }
    public function processCheckout(Request $request){
        if(Cart::isEmpty()){
            return redirect('/cart');
        }

        $request->validate([
            'name'=> 'required',
            'email'=>'required|email',
            'number'=> 'required',
            'address'=>'required',
        ]);
        $data=collect([]);
        
        $data->push([
            'billing_address' => $request->address,
            'billing_phone'=>$request->number,
            'billing_name' => $request->name,
            'billing_email' => $request->email,
        ]);
        
        session()->forget('checkout_data');
        session()->put('checkout_data',$data);
        return redirect(route('payment'))->with('success','pay');
    }

    protected function login(Request $request){
        $request->validate([
            'email' => 'required',
            'password'=>'required',
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
    
        if (Auth::attempt($credentials)) {
            notify()->success('Bạn đã đăng nhập');
            return back();
        }
    
        notify()->error('Đăng nhập thất bại');
        return back();
    }
}
