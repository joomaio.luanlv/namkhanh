<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\Category;

class ShopPageController extends Controller
{
    public function index(){
        $products=Product::latest()->paginate(9);
        $categories=Category::all();
        $featured_products=Product::where('featured',1)->latest()->take(4)->get();
        return view('shop',compact('products','categories','featured_products'));
    }
    public function search(Request $request){
        $validator=Validator::make($request->all(),[
            'query'=>'nullable|min:3',
        ]);

        if($validator->fails()){
            notify()->error('Tìm kiếm yêu cầu tối thiểu 3 ký tự');
            return back();
        }

        $query=$request->input('query');
        $catId=$request->input('catId');
        $from=$request->input('from')??0;
        $to=$request->input('to')??999999999999;
        
        // \print_r($request->all());die;
        if($catId) {
            $products=Product::query()
            ->where('title', 'LIKE', "%{$query}%")
            ->where('discounted_price', '>=', $from)
            ->where('discounted_price', '<=', $to)
            ->whereIn('category_id',$catId)
            ->paginate(12);
        }else {
            $products=Product::query()
            ->where('title', 'LIKE', "%{$query}%")
            ->where('discounted_price', '>=', $from)
            ->where('discounted_price', '<=', $to)
            ->paginate(12);
        }
        

        $categories=Category::all();
        $featured_products=Product::where('featured',1)->latest()->take(4)->get();
        return view('shop',compact('products','categories','featured_products'));
    }
    public function category(Request $request){
        $category=Category::where('slug',$request->category)->first();

        $query=$request->input('query');
        if($query){
            $products=Product::query()
            ->where('category_id',$category->id)
            ->where('title', 'LIKE', "%{$query}%") 
            ->paginate(10);
        }
        else{
            $products=Product::where('category_id',$category->id)->paginate(10);
        }
        $categories=Category::all();

        $cat_name=$category->name;
        return view('shop',compact('products','categories','cat_name'));
    }
}
