<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Products extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\Product::count();
        $string = 'Sản phẩm';
        $widget_name='products';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-company',
            'title'  => "{$count} {$string}",
            'text'   => __('Bạn có '.$count.' sản phẩm trong cơ sở dữ liệu. Bấm vào nút bên dưới để xem tất cả sản phẩm.', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => 'Xem tất cả sản phẩm',
                'link' => route('voyager.'.$widget_name.'.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/01.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        $product=\App\Product::first();
        return Auth::user()->can('browse',$product);
    }
}
